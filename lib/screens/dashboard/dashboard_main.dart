import 'package:flutter/material.dart';
import 'package:hero_transformation/components/level_progress_bar.dart';
import 'package:hero_transformation/components/expanded_content_block.dart';
import 'package:hero_transformation/components/recent_diary_block.dart';
import 'package:hero_transformation/components/recent_weekly_key_block.dart';

class DashboardMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Hero(
            tag: 'logo',
            child: Container(
              child: Image.asset('images/logo.png'),
              height: 100.0,
            ),
          ),
          SizedBox(
            height: 25.0,
          ),
          ExpandedContentBlock(
            title: 'Daily Inspiration',
            blockText: '"What You Lack In Talent Can Be Made Up With Desire, Hustle And Giving 110% All The Time." - Don Zimmer',
            blockTextColour: Colors.blueGrey,
            blockTextSize: 15.0,
          ),
          RecentWeeklyKeyBlock(
            title: 'Weekly Keys',
            blockText: 'Spend 15 minutes phoning your brother for a chat this week about a new project your working on',
            blockTextColour: Colors.blueGrey,
            blockTextSize: 15.0,
          ),
          SizedBox(
            height: 20.0,
          ),
          RecentDiaryBlock(
            title: 'Diary',
            blockText: 'You have a holiday booked on Thursday with your Wife to Tenerife, remember to pack on Wednesday',
            blockTextColour: Colors.blueGrey,
            blockTextSize: 15.0,
          ),
          SizedBox(
            height: 10.0,
          ),
          LevelProgressBar(),
          SizedBox(
            height: 30.0,
          ),
        ],
      ),
    );
  }
}