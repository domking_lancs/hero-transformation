import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/main_screen.dart';
import 'package:hero_transformation/components/expanded_settings_block.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

class SettingsScreen extends StatefulWidget {
  static const String id = 'settings_screen';

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pushNamed(context, MainScreen.id);
          },
        ),
        backgroundColor: Colors.white,
        title: Text(
          'Profile',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            FutureBuilder(
              future: Provider.of(context).auth.getCurrentUser(),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done){
                  return displayUserInformation(context, snapshot);
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  // Displays the user information and the expanded block will allow you to process the data and send to Firebase
  Widget displayUserInformation(context, snapshot){
    final user = snapshot.data;
    return ExpandedSettingsBlock(
      blockText: '${user.email}',
      blockTextColour: Colors.blueGrey,
    );
  }

}
