import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:hero_transformation/models/WeeklyKey.dart';
import 'package:intl/intl.dart';

class ShowKeyView extends StatelessWidget {

  final db = Firestore.instance;
  final WeeklyKey weeklyKey;

  ShowKeyView({Key key, this.weeklyKey}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Weekly Keys'),
        ),
        body: Center(
          child: StreamBuilder(
            stream: getUsersDiaryStreamSnapshots(context),
            builder: (context, snapshot) {
              if(snapshot.data == null) return CircularProgressIndicator();
              if(snapshot.data.documents.length == 0){
                return Text('You have no weekly key entries');
              } else {
                return new ListView.builder(
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (BuildContext context, int index) =>
                        buildDiaryCard(context, snapshot.data.documents[index]));
              }
            },
          ),
        )
    );
  }

  Stream<QuerySnapshot> getUsersDiaryStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'keys').snapshots();
  }

  Widget buildDiaryCard(BuildContext context, DocumentSnapshot keys) {
    String activity;
    Icon icon;

    if(keys['weeklyKeyActivity'] != null){
      icon = Icon(Icons.vpn_key, color: Colors.orangeAccent.shade200);
      activity = keys['weeklyKeyActivity'].toString();
    } else {
      icon = Icon(Icons.vpn_key, color: Colors.orangeAccent.shade200);
      activity = keys['weeklyKeyAction'].toString();
    }

    return new Container(
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child:
            ListTile(
              leading: icon,
              title: Text(activity),
              subtitle: Text("Dates: ${DateFormat('dd/MM/yyyy').format(keys['startDate'].toDate()).toString()} - ${DateFormat('dd/MM/yyyy').format(keys['endDate'].toDate()).toString()}"),
            ),
          ),
        )
    );
  }
}