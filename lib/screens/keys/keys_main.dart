import 'package:flutter/material.dart';
import 'package:hero_transformation/components/rounded_button.dart';
import 'package:hero_transformation/models/WeeklyKey.dart';
import 'package:hero_transformation/screens/keys/add_key_view.dart';
import 'package:hero_transformation/screens/keys/show_key_view.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

class KeysMain extends StatelessWidget {

  final db = Firestore.instance;
  final WeeklyKey weeklyKey;
  var today = new DateTime.now();

  KeysMain({Key key, this.weeklyKey}) : super(key: key);

  final newKey = new WeeklyKey(null, null, null, null, null);

  @override
  Widget build(BuildContext context) {

    return Container(
      child: StreamBuilder(
        stream: getUsersTripsStreamSnapshots(context),
        builder: (context, snapshot) {
          int itemCount = 0;
          if (snapshot.data == null) return CircularProgressIndicator();
          if (snapshot.data.documents != null) {
            itemCount = snapshot.data.documents.length;
          }
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Hero(
                  tag: 'logo',
                  child: Container(
                    child: Image.asset('images/logo.png'),
                    height: 100.0,
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                      'This section is for your 3 weekly keys, please fill out each of your weekly keys below. If you wish to view them please click on the green view keys button.'
                  ),
                ),
                AddWeeklyKeys(itemCount: itemCount, newKey: newKey),
                ShowEntries(itemCount: itemCount, weeklyKey: weeklyKey),
              ],
            ),
          );
        }),
    );
  }

  Stream<QuerySnapshot> getUsersTripsStreamSnapshots(
      BuildContext context) async* {
    var weeklyCheck = today.subtract(Duration(days: 7));
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'keys').where("startDate", isGreaterThan: weeklyCheck).snapshots();
  }
}

class AddWeeklyKeys extends StatelessWidget {

  final int itemCount;
  final WeeklyKey newKey;

  AddWeeklyKeys({this.itemCount, this.newKey});

  @override
  Widget build(BuildContext context) {
    if(itemCount < 3) {
      return RoundedButton(
          title: '+ Add Weekly Key',
          colour: Colors.redAccent.shade200,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) =>
                    AddKeyView(weeklyKey: newKey,)));
          }
      );
    } else {
      return Container();
    }
  }
}


class ShowEntries extends StatelessWidget {

  final int itemCount;
  final WeeklyKey weeklyKey;

  ShowEntries({this.itemCount, this.weeklyKey});

  @override
  Widget build(BuildContext context) {
    if(itemCount != 0){
      return RoundedButton(
          title: 'Show $itemCount Weekly Entries',
          colour: Colors.green.shade200,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ShowKeyView(weeklyKey: weeklyKey,)));
          }
      );
    } else {
      return Container();
    }
  }
}