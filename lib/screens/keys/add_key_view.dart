import 'package:flutter/material.dart';
import 'package:hero_transformation/models/WeeklyKey.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:hero_transformation/screens/keys/key_confirmation_view.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class AddKeyView extends StatefulWidget {
  final WeeklyKey weeklyKey;

  AddKeyView({Key key, @required this.weeklyKey}) : super(key: key);

  @override
  _AddKeyViewState createState() => _AddKeyViewState();
}

class _AddKeyViewState extends State<AddKeyView>
{
  final newKey = new WeeklyKey(null, null, null, null, null);

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now().add(Duration(days: 1));
  String _weeklyKeyActivity;
  String _weeklyKeyAction;
  String _weeklyKeyProcess;

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: _startDate,
        initialLastDate: _endDate,
        firstDate: new DateTime(DateTime.now().year - 50),
        lastDate: new DateTime(DateTime.now().year + 50));
    if (picked != null && picked.length == 2) {
      setState(() {
        _startDate = picked[0];
        _endDate = picked[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Add a Weekly Key'),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Select Dates"),
                  onPressed: () async {
                    await displayDateRangePicker(context);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Start Date: ${DateFormat('dd/MM/yyyy').format(_startDate).toString()}"),
                      Text("End Date: ${DateFormat('dd/MM/yyyy').format(_endDate).toString()}"),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "What will you do?"
                              ),
                              onChanged: (value) => _weeklyKeyActivity = value,
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "How will you do it?"
                              ),
                              onChanged: (value) => _weeklyKeyAction = value,
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Why will you do it?"
                              ),
                              onChanged: (value) => _weeklyKeyProcess = value,
                            )
                        )
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: RaisedButton(
                    child: Text("Continue"),
                    onPressed: () {
                      widget.weeklyKey.startDate = _startDate;
                      widget.weeklyKey.endDate = _endDate;
                      widget.weeklyKey.weeklyKeyActivity = _weeklyKeyActivity;
                      widget.weeklyKey.weeklyKeyAction = _weeklyKeyAction;
                      widget.weeklyKey.weeklyKeyProcess = _weeklyKeyProcess;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                KeyConfirmationView(weeklyKey: widget.weeklyKey)),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}