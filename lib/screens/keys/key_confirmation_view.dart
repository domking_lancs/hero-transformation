import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/models/WeeklyKey.dart';
import 'package:hero_transformation/screens/main_screen.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:intl/intl.dart';

class KeyConfirmationView extends StatelessWidget {
  final db = Firestore.instance;
  final WeeklyKey weeklyKey;

  KeyConfirmationView({Key key, @required this.weeklyKey}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Create Weekly Key Entry'),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Weekly Key Confirmation:",
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                Text(""),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Start Date: ${DateFormat('dd/MM/yyyy').format(weeklyKey.startDate).toString()}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("End Date: ${DateFormat('dd/MM/yyyy').format(weeklyKey.endDate).toString()}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Weekly Key Activity: ${weeklyKey.weeklyKeyActivity}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Weekly Key Action: ${weeklyKey.weeklyKeyAction}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Weekly Key Process: ${weeklyKey.weeklyKeyProcess}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: RaisedButton(
                    child: Text("Submit"),
                    onPressed: () async {
                      // save data to firebase
                      final uid = await Provider.of(context).auth.getCurrentUID();
                      await db.collection("userData").document(uid).collection("keys").add(weeklyKey.toJson());
                      Navigator.pushNamed(context, MainScreen.id);
                    },
                  ),
                ),
              ],
            )
        )
    );
  }
}