import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/calendar/calendar_main.dart';
import 'package:hero_transformation/screens/diary/diary_main.dart';
import 'package:hero_transformation/screens/keys/keys_main.dart';
import 'package:hero_transformation/screens/progress/progress_main.dart';
import 'package:hero_transformation/screens/settings_screen.dart';
import 'package:hero_transformation/screens/dashboard/dashboard_main.dart';
import 'package:hero_transformation/services/auth_service.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

class MainScreen extends StatefulWidget {
  static const String id = 'main_screen';

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
{
  /* The main screen loads in dynamic content depending on the bottom navigation tab when pressed
  for example: when clicking on the calendar icon it will load CalendarMain() content */
  int _currentIndex = 0;
  final List<Widget> _children = [
    DashboardMain(),
    CalendarMain(),
    ProgressMain(),
    KeysMain(),
    DiaryMain(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.person,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pushNamed(context, SettingsScreen.id);
            },
          ),
          backgroundColor: Colors.white,
          title: Text(
            'Hero Transformation',
            style: TextStyle(color: Colors.black),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                Icons.exit_to_app,
                color: Colors.redAccent,
              ),
              onPressed: () async {
                try {
                  AuthService auth = Provider.of(context).auth;
                  await auth.signOut();
                } catch(e) {
                  print(e);
                }
              },
            )
          ],
        ),
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.shifting,
          selectedItemColor: Colors.redAccent,
          unselectedItemColor: Colors.black,
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.home),
              title: new Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.calendar_today),
              title: Text('Daily View'),
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.trending_up),
                title: Text('Progress')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.vpn_key),
                title: Text('Weekly Keys')
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.book),
                title: Text('Diary')
            )
          ],
        )
    );
  }

  // when the bottom tab is pressed in the footer it will load in dynamic data within the list for each view
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}