import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/diary/show_diary_view.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:hero_transformation/models/Diary.dart';
import 'package:hero_transformation/components/alert_dialog.dart';
import 'package:hero_transformation/components/rounded_button.dart';

class DiaryMain extends StatelessWidget {

  final db = Firestore.instance;
  final Diary diary;
  var today = new DateTime.now();

  DiaryMain({Key key, this.diary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
          stream: getUsersTripsStreamSnapshots(context),
          builder: (context, snapshot) {
            int itemCount = 0;
            if(snapshot.data == null) return CircularProgressIndicator();
            if(snapshot.data.documents != null){
              itemCount = snapshot.data.documents.length;
            }
            return Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Hero(
                      tag: 'logo',
                      child: Container(
                        child: Image.asset('images/logo.png'),
                        height: 100.0,
                      ),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                          'Here you will have the ability to schedule time within your diary. This can be for keeping a personal diary of activities throughout the week. To view your diary click on the green button to view your weekly schedule.'
                      ),
                    ),
                    RoundedButton(
                        title: '+ Add Diary Entry',
                        colour: Colors.redAccent.shade200,
                        onPressed: () {
                          showAlertDialog(context);
                        }
                    ),
                    ShowEntries(itemCount: itemCount, diary: diary),
                  ],
                ),
              );
          },
      ),
    );
  }

  Stream<QuerySnapshot> getUsersTripsStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'diary').where("endDate", isGreaterThan: today).snapshots();
  }
}

class ShowEntries extends StatelessWidget {

  final int itemCount;
  final Diary diary;

  ShowEntries({this.itemCount, this.diary});

  @override
  Widget build(BuildContext context) {
    if(itemCount != 0){
      return RoundedButton(
          title: 'Show $itemCount Diary Entries',
          colour: Colors.green.shade200,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => ShowDiaryView(diary: diary,)));
          }
      );
    } else {
      return Container();
    }
  }
}