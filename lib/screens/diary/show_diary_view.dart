import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:hero_transformation/models/Diary.dart';
import 'package:intl/intl.dart';

class ShowDiaryView extends StatelessWidget {

  final db = Firestore.instance;
  final Diary diary;

  ShowDiaryView({Key key, this.diary}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent.shade700,
        title: Text('Diary'),
      ),
      body: Center(
      child: StreamBuilder(
        stream: getUsersDiaryStreamSnapshots(context),
        builder: (context, snapshot) {
          if(snapshot.data == null) return CircularProgressIndicator();
          if(snapshot.data.documents.length == 0){
            return Text('You have no diary entries');
          } else {
            return new ListView.builder(
                itemCount: snapshot.data.documents.length,
                itemBuilder: (BuildContext context, int index) =>
                    buildDiaryCard(context, snapshot.data.documents[index]));
          }
        },
      ),
    )
    );
  }

  Stream<QuerySnapshot> getUsersDiaryStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'diary').snapshots();
  }

  Widget buildDiaryCard(BuildContext context, DocumentSnapshot diary) {
    String activity;
    Icon icon;

    if(diary['freeTimeActivity'] != null){
      icon = Icon(Icons.wb_sunny, color: Colors.orangeAccent.shade200);
      activity = diary['freeTimeActivity'].toString();
    } else if(diary['freeTimeActivity'] == null && diary['projectTitle'] != null){
      icon = Icon(Icons.business_center, color: Colors.orangeAccent.shade200);
      activity = diary['projectTitle'].toString();
    } else {
      icon = Icon(Icons.supervisor_account, color: Colors.orangeAccent.shade200);
      activity = diary['relationshipName'].toString();
    }

    return new Container(
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
          ListTile(
            leading: icon,
            title: Text(activity),
            subtitle: Text("Dates: ${DateFormat('dd/MM/yyyy').format(diary['startDate'].toDate()).toString()} - ${DateFormat('dd/MM/yyyy').format(diary['endDate'].toDate()).toString()}"),
          ),
        ),
      )
    );
  }
}