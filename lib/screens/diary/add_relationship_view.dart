import 'package:flutter/material.dart';
import 'package:hero_transformation/models/Diary.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:hero_transformation/screens/diary/relationship_confirmation_view.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class AddRelationshipView extends StatefulWidget {
  final Diary diary;

  AddRelationshipView({Key key, @required this.diary}) : super(key: key);

  @override
  _AddRelationshipViewState createState() => _AddRelationshipViewState();
}

class _AddRelationshipViewState extends State<AddRelationshipView> {

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now().add(Duration(days: 1));
  String _relationshipName;
  String _relationshipResult;

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: _startDate,
        initialLastDate: _endDate,
        firstDate: new DateTime(DateTime.now().year - 50),
        lastDate: new DateTime(DateTime.now().year + 50));
    if (picked != null && picked.length == 2) {
      setState(() {
        _startDate = picked[0];
        _endDate = picked[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Add a Relationship'),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Select Dates"),
                  onPressed: () async {
                    await displayDateRangePicker(context);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Start Date: ${DateFormat('dd/MM/yyyy').format(_startDate).toString()}"),
                      Text("End Date: ${DateFormat('dd/MM/yyyy').format(_endDate).toString()}"),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(30.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Relationship Name"
                              ),
                              onChanged: (value) => _relationshipName = value,
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(30.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Relationship Result"
                              ),
                              onChanged: (value) => _relationshipResult = value,
                            )
                        )
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: RaisedButton(
                    child: Text("Continue"),
                    onPressed: () {
                      widget.diary.startDate = _startDate;
                      widget.diary.endDate = _endDate;
                      widget.diary.relationshipName = _relationshipName;
                      widget.diary.relationshipResult = _relationshipResult;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                RelationshipConfirmationView(diary: widget.diary)),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}