import 'package:flutter/material.dart';
import 'package:hero_transformation/models/Diary.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:hero_transformation/screens/diary/project_confirmation_view.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class AddProjectView extends StatefulWidget {
  final Diary diary;

  AddProjectView({Key key, @required this.diary}) : super(key: key);

  @override
  _AddProjectViewState createState() => _AddProjectViewState();
}

class _AddProjectViewState extends State<AddProjectView> {

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now().add(Duration(days: 1));
  String _projectTitle;
  String _projectAction;
  String _projectParticipants;

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: _startDate,
        initialLastDate: _endDate,
        firstDate: new DateTime(DateTime.now().year - 50),
        lastDate: new DateTime(DateTime.now().year + 50));
    if (picked != null && picked.length == 2) {
      setState(() {
        _startDate = picked[0];
        _endDate = picked[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Add a Project'),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Select Dates"),
                  onPressed: () async {
                    await displayDateRangePicker(context);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Start Date: ${DateFormat('dd/MM/yyyy').format(_startDate).toString()}"),
                      Text("End Date: ${DateFormat('dd/MM/yyyy').format(_endDate).toString()}"),
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Project Title"
                              ),
                              onChanged: (value) => _projectTitle = value,
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Project Actions"
                              ),
                              onChanged: (value) => _projectAction = value,
                            )
                        )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: TextField(
                              decoration: InputDecoration(
                                  helperText: "Project Participants"
                              ),
                              onChanged: (value) => _projectParticipants = value,
                            )
                        )
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: RaisedButton(
                    child: Text("Continue"),
                    onPressed: () {
                      widget.diary.startDate = _startDate;
                      widget.diary.endDate = _endDate;
                      widget.diary.projectTitle = _projectTitle;
                      widget.diary.projectAction = _projectAction;
                      widget.diary.projectParticipants = _projectParticipants;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                              ProjectConfirmationView(diary: widget.diary)),
                      );
                    },
                  ),
                ),
              ],
            )));
  }
}