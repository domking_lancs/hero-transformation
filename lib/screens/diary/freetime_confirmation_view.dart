import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/screens/main_screen.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:hero_transformation/models/Diary.dart';
import 'package:intl/intl.dart';

class FreeTimeConfirmationView extends StatelessWidget {
  final db = Firestore.instance;

  final Diary diary;
  FreeTimeConfirmationView({Key key, @required this.diary}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.redAccent.shade700,
          title: Text('Create Diary Entry'),
        ),
        body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Diary Entry Confirmation:",
                  style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
                Text(""),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Start Date: ${DateFormat('dd/MM/yyyy').format(diary.startDate).toString()}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("End Date: ${DateFormat('dd/MM/yyyy').format(diary.endDate).toString()}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Text("Free Time Activity: ${diary.freeTimeActivity}"),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: RaisedButton(
                    child: Text("Submit"),
                    onPressed: () async {
                      // save data to firebase
                      final uid = await Provider.of(context).auth.getCurrentUID();
                      await db.collection("userData").document(uid).collection("diary").add(diary.toJson());
                      Navigator.pushNamed(context, MainScreen.id);
                    },
                  ),
                ),
              ],
            )
        )
    );
  }
}