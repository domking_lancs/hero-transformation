import 'package:flutter/material.dart';
import 'package:hero_transformation/components/level_progress_bar.dart';
import 'package:hero_transformation/components/rounded_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:hero_transformation/models/Score.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:hero_transformation/components/help_dialog.dart';
import 'package:hero_transformation/screens/progress/monthly_progress.dart';

class ProgressMain extends StatefulWidget {

  final BeautifulPopup popup;

  ProgressMain({this.popup});

  @override
  ProgressMainState createState() => new ProgressMainState();
}

class ProgressMainState extends State<ProgressMain>
{
  final db = Firestore.instance;
  Score dailyScore = new Score(null, null);

  // This will send a value of zero as a score to firebase for a weekly streak allowing a day off
  sendWeeklyStreak()
  {
      int value = 0;
      dailyScore.dailyScore = value.toString();
      _sendScore();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Hero(
            tag: 'logo',
            child: Container(
              child: Image.asset('images/logo.png'),
              height: 100.0,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
                'Here you have the ability to monitor your progress. You will have the ability to use your weekly streak once a week by clicking the weekly streak button.'
            ),
          ),
          LevelProgressBar(),
          IconButton(
            icon: Icon(
              Icons.info,
              color: Colors.black,
            ),
            onPressed: () {
              showHelpDialog(context);
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text('Your weekly progress chart:'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SizedBox(
                  height: 40.0,
                  child: BuildProgressChart(),
                ),
              ),
            ],
          ),
          RoundedButton(
              title: 'Use your Weekly Streak',
              colour: Colors.grey.shade500,
              onPressed: () {
                sendWeeklyStreak();
              }
          ),
          RoundedButton(
              title: 'View your Monthly Progress',
              colour: Colors.redAccent.shade700,
              onPressed: () {
                Navigator.pushNamed(context, MonthlyProgress.id);
              }
          ),
        ],
      ),
    );
  }

  // send the score to firebase when weekly streak button is pressed
  _sendScore() async {
    final uid = await Provider.of(context).auth.getCurrentUID();
    await db.collection("userData").document(uid).collection("score").add(dailyScore.toJson());
  }

}

class BuildProgressChart extends StatelessWidget {

  final Score score;

  BuildProgressChart({Key key, this.score}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: getUsersScoreStreamSnapshots(context),
      builder: (context, snapshot) {
        if(snapshot.data == null) return CircularProgressIndicator();
        if(snapshot.data.documents.length == 0){
          return Center(child: Text('You have no recorded scores'));
        } else {
          return Column(
            children: <Widget>[
              SizedBox(
                height: 40, // constrain height
                child: ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: 200, minHeight: 56.0),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (BuildContext context, int index) =>
                          buildProgressChart(context, snapshot.data.documents[index])
                      )
                  ),
                ),
            ],
          );
        }
      },
    );
  }

  // Stream the users score, it required a stream of snapshots because it constantly needs to bring in new scores
  Stream<QuerySnapshot> getUsersScoreStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'score').limit(7).snapshots();
  }

  /* The chart will be increase up to 7, showing the week of submissions - below is a simple if statement to change the
  colour - this could be further streamlined into an array and made smarter for progress viewing
   */
  Widget buildProgressChart(BuildContext context, DocumentSnapshot score)
  {
    Color scoreColour;

    // if statements to assign a colour to the blocks depending on the score
    if(score['score'] == "0"){
      scoreColour = Colors.grey;
    } if (score['score'] == "1" || score['score'] == "2" || score['score'] == "3" || score['score'] == "4") {
      scoreColour = Colors.yellow;
    } if(score['score'] == "5"){
      scoreColour = Colors.green;
    }

    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: new ConstrainedBox(
          constraints: new BoxConstraints(
            minHeight: 30.0,
            minWidth: 30.0,
            maxHeight: 50.0,
            maxWidth: 50.0,
          ),
          child: new DecoratedBox(
            decoration: new BoxDecoration(color: scoreColour),
          )
      ),
    );
  }
}