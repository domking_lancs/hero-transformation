import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/main_screen.dart';
import 'package:hero_transformation/components/progress_chart_view.dart';

class MonthlyProgress extends StatefulWidget {
  static const String id = 'monthly_progress';

  @override
  _MonthlyProgressState createState() => _MonthlyProgressState();
}

// This is used to process the monthly progress graph - needs dynamic data pulling through
class _MonthlyProgressState extends State<MonthlyProgress>
{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.pushNamed(context, MainScreen.id);
          },
        ),
        backgroundColor: Colors.white,
        title: Text(
          'Monthly Progress Chart',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
        child: ProgressChartView()
      ),
    );
  }
}
