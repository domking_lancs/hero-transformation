import 'package:flutter/material.dart';
import 'package:hero_transformation/components/rounded_button.dart';
import 'sign_up_screen.dart';

class WelcomeScreen extends StatefulWidget {
  static const String id = 'welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Hero(
              tag: 'logo',
              child: Container(
                child: Image.asset('images/logo.png'),
                height: 200.0,
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            RoundedButton(
                title: 'Login',
                colour: Colors.redAccent,
                onPressed: () {
                  Navigator.pushNamed(context, SignUpScreen.signin_id);
                }),
            RoundedButton(
                title: 'Register',
                colour: Colors.redAccent,
                onPressed: () {
                  Navigator.pushNamed(context, SignUpScreen.signup_id);
                }),
          ],
        ),
      )
    );
  }
}
