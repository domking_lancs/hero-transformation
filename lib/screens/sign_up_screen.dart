import 'package:flutter/material.dart';
import 'package:hero_transformation/components/rounded_button.dart';
import 'package:hero_transformation/constants.dart';
import 'package:hero_transformation/screens/main_screen.dart';
import 'package:hero_transformation/services/auth_service.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

enum AuthFormType {signIn, signUp, reset}

class SignUpScreen extends StatefulWidget {
  static const String signup_id = 'sign_up_screen';
  static const String signin_id = 'sign_in_screen';

  final AuthFormType authFormType;

  SignUpScreen({Key key, @required this.authFormType}) : super(key:key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState(authFormType: this.authFormType);
}

class _SignUpScreenState extends State<SignUpScreen>
{
  /* The Auth type is used so that it uses less screens - a smarter way to reuse screens depending on the type
  essentially hiding and showing elements
   */
  AuthFormType authFormType;
  _SignUpScreenState({this.authFormType});

  final formKey = GlobalKey<FormState>();
  String email, password, warning;

  void switchFormState(String state) {
    formKey.currentState.reset();
    if(state == 'signUp') {
      setState(() {
        authFormType = AuthFormType.signUp;
      });
    } else {
      setState(() {
        authFormType = AuthFormType.signIn;
      });
    }
  }

  bool validate() {
    final form = formKey.currentState;
    form.save();
    if(form.validate()){
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void submit() async {
    if(validate()) {
      try {
        final auth = Provider
            .of(context)
            .auth;
        if (authFormType == AuthFormType.signIn) {
          String uid = await auth.signInWithEmailAndPassword(email, password);
          print("signed in with id $uid");
          Navigator.pushNamed(context, MainScreen.id);
        } else if(authFormType == AuthFormType.reset){
          await auth.sendPasswordResetEmail(email);
          print("Password reset email sent");
          warning = "A password reset link has been sent to $email";
          setState(() {
            authFormType = AuthFormType.signIn;
          });
        }else {
          String uid = await auth.createUserWithEmailAndPassword(email, password);
          print("signed up with new id $uid");
          Navigator.pushNamed(context, MainScreen.id);
        }
      } catch (e) {
        setState(() {
          warning = e.message;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Flexible(
              child: Hero(
                tag: 'logo',
                child: Container(
                  height: 200.0,
                  child: Image.asset('images/logo.png'),
                ),
              ),
            ),
            SizedBox(
              height: 48.0,
            ),
            Form(
              key: formKey,
              child: Column(
                children: buildInputs() + buildRoundedButton(),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showAlert(){
    if(warning != null){
      return Container(
        color: Colors.amberAccent.shade200,
        width: double.infinity,
        padding: EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Icon(Icons.error_outline),
            ),
            Expanded(child: Text(warning, maxLines: 3,)),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  setState(() {
                    warning = null;
                  });
                },
              ),
            )
          ],
        )
      );
    }
    return SizedBox(height: 0);
  }

  List<Widget> buildInputs() {
    List<Widget> textFields = [];

    if (authFormType == AuthFormType.reset) {
      textFields.add(
          TextFormField(
            validator: EmailValidator.validate,
            decoration: kTextFieldDecoration.copyWith(hintText: 'Enter your Email'),
            onSaved: (value) => email = value,
          )
      );
      textFields.add(SizedBox(height: 10.0));
      return textFields;
    }

    textFields.add(
        TextFormField(
          validator: EmailValidator.validate,
          decoration: kTextFieldDecoration.copyWith(hintText: 'Enter your Email'),
          onSaved: (value) => email = value,
        )
    );
    textFields.add(SizedBox(height: 10.0));
    textFields.add(
        TextFormField(
          validator: PasswordValidator.validate,
          decoration: kTextFieldDecoration.copyWith(hintText: 'Enter your Password'),
          obscureText: true,
          onSaved: (value) => password = value,
        )
    );
    textFields.add(SizedBox(height: 10.0));

    return textFields;
  }

  List<Widget> buildRoundedButton(){
    String _switchButtonText, _submitButtonText, _newFormState;
    bool _showForgotPassword = false;

    if(authFormType == AuthFormType.signIn) {
      _switchButtonText = "Create a New Account";
      _submitButtonText = "Login";
      _newFormState = "signUp";
      _showForgotPassword = true;
    } else if(authFormType == AuthFormType.reset){
      _switchButtonText = "Return to Sign In";
      _submitButtonText = "Submit";
      _newFormState = "signIn";
    } else {
      _switchButtonText = "Already have an Account? Sign In";
      _submitButtonText = "Register";
      _newFormState = "signIn";
    }

    return [
      showAlert(),
      RoundedButton(
        title: _submitButtonText,
        colour: Colors.redAccent,
        onPressed: () {
          submit();
        },
      ),
      showForgotPassword(_showForgotPassword),
      FlatButton(
        child: Text(
          _switchButtonText
        ),
        onPressed: () {
          switchFormState(_newFormState);
        },
      )
    ];
  }

  Widget showForgotPassword(bool visible){
    return Visibility(
      child: FlatButton(
        child: Text('Forgot Password?'),
        onPressed: () {
          setState(() {
            authFormType = AuthFormType.reset;
          });
        }
      ),
      visible: visible,
    );
  }
}
