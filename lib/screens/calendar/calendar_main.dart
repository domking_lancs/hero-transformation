import 'package:flutter/material.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:hero_transformation/components/five_weekly_things_view.dart';

class CalendarMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final popup = BeautifulPopup(
      context: context,
      template: TemplateTerm,
    );

    // returns the checkbox view in a component to avoid any logic being processed here
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
      child: FiveWeeklyThingsView(
        popup: popup
      ),
    );
  }
}