class User {
  String displayName;
  String telephone;
  String address;
  bool admin;

  User(this.displayName, this.telephone, this.address);

  Map<String, dynamic> toJson() => {
    'displayName': displayName,
    'telephone': telephone,
    'address': address,
    'admin': admin
  };
}