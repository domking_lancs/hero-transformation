class Score {
  DateTime date;
  String dailyScore;

  Score(this.date, this.dailyScore);

  Map<String, dynamic> toJson() => {
    'date': date,
    'score': dailyScore
  };
}