class Diary {
  DateTime startDate;
  DateTime endDate;
  String freeTimeActivity;
  String projectTitle;
  String projectAction;
  String projectParticipants;
  String relationshipName;
  String relationshipResult;

  Diary(
    this.startDate,
    this.endDate,
    this.freeTimeActivity,
    this.projectTitle,
    this.projectAction,
    this.projectParticipants,
    this.relationshipName,
    this.relationshipResult
  );

  Map<String, dynamic> toJson() => {
    'startDate': startDate,
    'endDate': endDate,
    'freeTimeActivity': freeTimeActivity,
    'projectTitle': projectTitle,
    'projectAction': projectAction,
    'projectParticipants': projectParticipants,
    'relationshipName': relationshipName,
    'relationshipResult': relationshipResult
  };
}
