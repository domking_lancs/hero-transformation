class WeeklyKey {
  DateTime startDate;
  DateTime endDate;
  String weeklyKeyActivity;
  String weeklyKeyAction;
  String weeklyKeyProcess;

  WeeklyKey(
    this.startDate,
    this.endDate,
    this.weeklyKeyActivity,
    this.weeklyKeyAction,
    this.weeklyKeyProcess
  );

  Map<String, dynamic> toJson() => {
    'startDate': startDate,
    'endDate': endDate,
    'weeklyKeyActivity': weeklyKeyActivity,
    'weeklyKeyAction': weeklyKeyAction,
    'weeklyKeyProcess': weeklyKeyProcess
  };
}