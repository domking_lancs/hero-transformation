import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/progress/monthly_progress.dart';
import 'package:hero_transformation/screens/settings_screen.dart';
import 'package:hero_transformation/screens/sign_up_screen.dart';
import 'package:hero_transformation/screens/welcome_screen.dart';
import 'package:hero_transformation/services/auth_service.dart';
import 'screens/main_screen.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      auth: AuthService(),
      db: Firestore.instance,
      child: MaterialApp(
        initialRoute: WelcomeScreen.id,
        title: 'Hero Transformation',
        theme: ThemeData.light(),
        // routes are given an id - the auth type changes on signup in order to change the layout
        routes: {
          WelcomeScreen.id: (context) => WelcomeScreen(),
          SignUpScreen.signin_id: (context) => SignUpScreen(authFormType: AuthFormType.signIn),
          SignUpScreen.signup_id: (context) => SignUpScreen(authFormType: AuthFormType.signUp),
          MainScreen.id: (context) => HomeController(),
          SettingsScreen.id: (context) => SettingsScreen(),
          MonthlyProgress.id: (context) => MonthlyProgress(),
        },
      ),
    );
  }
}

/* This home controller is used to change the state, it will check if you are logged in
 if not you will be greeted with the main screen otherwise the welcome screen - this is used
 for a security perspective */
class HomeController extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AuthService auth = Provider.of(context).auth;
    return StreamBuilder(
      stream: auth.onAuthStateChanged,
      builder: (context, AsyncSnapshot<String> snapshot){
        if(snapshot.connectionState == ConnectionState.active) {
          final bool signedIn = snapshot.hasData;
          return signedIn ? MainScreen() : WelcomeScreen();
        }
        return CircularProgressIndicator();
      }
    );
  }
}