import 'package:flutter/material.dart';

// A break line component to split up data - you can see this in the settings screen
class HeadingBreakLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
            height: 50.0,
            width: 400.0,
            child: Divider(
                color: Colors.grey
            )
        ),
      ],
    );
  }
}