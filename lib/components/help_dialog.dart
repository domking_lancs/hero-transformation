import 'package:flutter/material.dart';
import 'rounded_button.dart';

// This is used to display the colour key for progress, basically re-used elements already incorporated
// can edit this to display other data or a different key view
showHelpDialog(BuildContext context)
{
  // set up the buttons
  Widget streakBreak = RoundedButton(
    title: "Streak Break",
    colour: Colors.grey,
    onPressed: () async {
        Navigator.of(context).pop();
    },
  );
  Widget partialComplete = RoundedButton(
    title: "Partially Completed",
    colour: Colors.yellowAccent.shade700,
    onPressed: () async {
      Navigator.of(context).pop();
    },
  );
  Widget completed = RoundedButton(
    title: "Completed",
    colour: Colors.green,
    onPressed: () async {
      Navigator.of(context).pop();
    },
  );
  Widget levelUp = RoundedButton(
    title: "Levelled Up",
    colour: Colors.blue,
    onPressed: () async {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    actions: [
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(child: Text('Streak Colour Guide:')),
      ),
      Center(child: streakBreak),
      Center(child: partialComplete),
      Center(child: completed),
      Center(child: levelUp),
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}