import 'package:flutter/material.dart';
import 'package:hero_transformation/screens/diary/add_project_view.dart';
import 'package:hero_transformation/screens/diary/add_relationship_view.dart';
import 'package:hero_transformation/screens/diary/free_time_view.dart';
import 'rounded_button.dart';
import 'package:hero_transformation/models/Diary.dart';

showAlertDialog(BuildContext context) {

  final newDiary = new Diary(null, null, null, null, null, null, null, null);

  // set up the buttons
  Widget remindButton = RoundedButton(
    title: "Schedule Free Time",
    colour: Colors.redAccent,
    onPressed: () async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => FreeTimeView(diary: newDiary)),
      );
    },
  );
  Widget cancelButton = RoundedButton(
    title: "Add Project",
    colour: Colors.redAccent,
    onPressed: () async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => AddProjectView(diary: newDiary)),
      );
    },
  );
  Widget launchButton = RoundedButton(
    title: "Add Relationship",
    colour: Colors.redAccent,
    onPressed: () async {
      Navigator.push(context, MaterialPageRoute(builder: (context) => AddRelationshipView(diary: newDiary)),
      );
    },  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    actions: [
      Center(child: remindButton),
      Center(child: cancelButton),
      Center(child: launchButton),
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}