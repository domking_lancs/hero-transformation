import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LevelProgressBar extends StatelessWidget
{
  final db = Firestore.instance;

  // Requires scores to be dynamic from Firebase and a sum calculated - can then create an array
  // for the level architecture, changing the levels depending on the scores
  @override
  Widget build(BuildContext context)
  {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: new LinearPercentIndicator(
        width: MediaQuery
            .of(context)
            .size
            .width - 70,
        animation: true,
        lineHeight: 20.0,
        animationDuration: 2500,
        percent: 0.7,
        center: Text('Level 1'),
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: Colors.blue,
      ),
    );
  }
}