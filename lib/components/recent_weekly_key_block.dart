import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:intl/intl.dart';

class RecentWeeklyKeyBlock extends StatelessWidget {

  RecentWeeklyKeyBlock({this.title, this.blockText, this.blockTextColour, this.blockTextSize});

  final db = Firestore.instance;
  final String title;
  final String blockText;
  final Color blockTextColour;
  final double blockTextSize;
  dynamic today = new DateTime.now();

  @override
  Widget build(BuildContext context) {
    today = new DateTime(today.year, today.month, today.day);
    return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: StreamBuilder(
                stream: getUsersRecentKeysStreamSnapshots(context),
                builder: (context, snapshot) {
                  if(snapshot.data == null) return CircularProgressIndicator();
                  if(snapshot.data.documents.length == 0){
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 15.0),
                      child: Text('You have no weekly key entries'),
                    );
                  } else {
                    return Expanded(
                      child: SizedBox(
                        height: 70.0,
                        child: Center(
                          child: new ListView.builder(
                              shrinkWrap: true,
                              itemCount: snapshot.data.documents.length,
                              itemBuilder: (BuildContext context, int index) =>
                                  getKeys(context, snapshot.data.documents[index])),
                        )
                      ),
                    );
                  }
                },
              ),
            )
          ],
    );
  }

  Stream<QuerySnapshot> getUsersRecentKeysStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'keys').where("endDate", isGreaterThan: today).limit(1).snapshots();
  }

  Widget getKeys(BuildContext context, DocumentSnapshot keys) {
    String activity;
    activity = keys['weeklyKeyActivity'].toString();
    print(activity);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Text(
        "$activity - To be completed between the dates: ${DateFormat('dd/MM/yyyy').format(keys['startDate'].toDate()).toString()} - ${DateFormat('dd/MM/yyyy').format(keys['endDate'].toDate()).toString()}",
        style: TextStyle(
            fontSize: blockTextSize ?? 12.0,
            color: blockTextColour
        ),
      ),
    );
  }
}