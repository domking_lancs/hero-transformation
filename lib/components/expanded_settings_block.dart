import 'package:flutter/material.dart';
import 'heading_break_line.dart';
import 'package:hero_transformation/models/User.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';

class ExpandedSettingsBlock extends StatefulWidget {

  ExpandedSettingsBlock({this.blockText, this.blockTextColour});
  final String blockText;
  final Color blockTextColour;

  @override
  _ExpandedSettingsBlockState createState() => _ExpandedSettingsBlockState();
}

class _ExpandedSettingsBlockState extends State<ExpandedSettingsBlock> {
  // user instance has been initialised in order to update and store user data
  User user = new User("", "", "");

  // added in a boolean for admin rights, this is incorporated for future work
  bool _isAdmin = false;

  // Text editing controller allows you to submit null values
  TextEditingController _userTelephoneController = new TextEditingController();
  TextEditingController _userDisplayNameController = new TextEditingController();
  TextEditingController _userAddressController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                CircleAvatar(
                  radius: 50.0,
                  backgroundImage: AssetImage('images/avatar.png'),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      FutureBuilder(
                        future: _getProfileData(),
                        builder: (context, snapshot) {
                          if(snapshot.connectionState == ConnectionState.done){
                            _userDisplayNameController.text = user.displayName;
                            _isAdmin = user.admin ?? false;
                          }
                          return Text(
                            user.displayName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0
                            ),
                          );
                        },
                      ),
                      Text(
                        widget.blockText,
                        style: TextStyle(
                          fontSize: 12.0,
                          color: widget.blockTextColour
                        ),

                      ),
                    ],
                  ),
                ),
              ],
            ),
            HeadingBreakLine(),
            FutureBuilder(
              future: _getProfileData(),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done){
                  _userTelephoneController.text = user.telephone;
                  _isAdmin = user.admin ?? false;
                }
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Telephone: ${user.telephone}",
                        ),
                      ),
                      adminFeature(),
                    ],
                  ),
                );
              },
            ),
            FutureBuilder(
              future: _getProfileData(),
              builder: (context, snapshot) {
                if(snapshot.connectionState == ConnectionState.done){
                  _userAddressController.text = user.address;
                  _isAdmin = user.admin ?? false;
                }
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Address: ${user.address}",
                        ),
                      ),
                      adminFeature(),
                    ],
                  ),
                );
              },
            ),
            Center(
              child: RaisedButton(
                child: Text('Edit Profile'),
                onPressed: () {
                  _userEditForm(context);
                },
              ),
            )
          ],
        ),
    );
  }

  // retrieve the profile data from Firebase
  _getProfileData() async {
    final uid = await Provider.of(context).auth.getCurrentUID();
    await Provider.of(context).db.collection('userData').document(uid).get().then((result){
      user.displayName = result.data['displayName'];
      user.telephone = result.data['telephone'];
      user.address = result.data['address'];
      user.admin = result.data['admin'];
    });
  }

  // This can be further extended going forward, just set up and admin feature for show now
  Widget adminFeature() {
    if(_isAdmin == true) {
      return Text("You are an admin");
    } else {
      return Container();
    }
  }

  // the pop up form to edit the data and send the data to Firebase
  void _userEditForm(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            height: MediaQuery.of(context).size.height * .60,
            child: Padding(
              padding: const EdgeInsets.only(left: 15.0, top: 15.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("Update Profile"),
                      Spacer(),
                      IconButton(
                        icon: Icon(Icons.cancel),
                        color: Colors.orange,
                        iconSize: 25,
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: TextField(
                                  controller: _userDisplayNameController,
                                  decoration: InputDecoration(
                                      helperText: "Display Name"
                                  )
                              )
                          )
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 15.0),
                          child: TextField(
                            controller: _userTelephoneController,
                            decoration: InputDecoration(
                              helperText: "Telephone"
                            )
                          )
                        )
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: TextField(
                                  controller: _userAddressController,
                                  decoration: InputDecoration(
                                      helperText: "Address"
                                  )
                              )
                          )
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        child: Text('Save'),
                        color: Colors.green,
                        textColor: Colors.white,
                        onPressed: () async {
                          user.displayName = _userDisplayNameController.text;
                          user.telephone = _userTelephoneController.text;
                          user.address = _userAddressController.text;
                          setState(() {
                            _userDisplayNameController.text = user.displayName;
                            _userTelephoneController.text = user.telephone;
                            _userAddressController.text = user.address;
                          });
                          final uid = await Provider.of(context).auth.getCurrentUID();
                          await Provider.of(context).db.collection('userData').document(uid).setData(user.toJson());
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  )
                ],
              ),
            )
          );
        }
    );
  }
}