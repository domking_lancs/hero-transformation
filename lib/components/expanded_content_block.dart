import 'package:flutter/material.dart';

class ExpandedContentBlock extends StatelessWidget {

  ExpandedContentBlock({this.title, this.blockText, this.blockTextColour, this.blockTextSize});

  final String title;
  final String blockText;
  final Color blockTextColour;
  final double blockTextSize;

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Text(
                blockText,
                style: TextStyle(
                    fontSize: blockTextSize ?? 12.0,
                    color: blockTextColour
                ),
              ),
            )
          ],
        )
    );
  }
}