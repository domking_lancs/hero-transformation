import 'package:flutter/material.dart';
import 'rounded_button.dart';
import 'package:flutter_beautiful_popup/main.dart';
import 'package:hero_transformation/models/Score.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/screens/main_screen.dart';

class FiveWeeklyThingsView extends StatefulWidget {
  final BeautifulPopup popup;

  FiveWeeklyThingsView({this.popup});

  @override
  _FiveWeeklyThingsViewState createState() => _FiveWeeklyThingsViewState();
}

class _FiveWeeklyThingsViewState extends State<FiveWeeklyThingsView>
{
  final db = Firestore.instance;
  Score dailyScore = new Score(null, null);

  /* Added all of the checkbox values into a map so can loop through each key
   and get all of the individual values */
  Map<String, bool> values = {
    'FOCUS': false,
    'FITNESS': false,
    'FAMILY': false,
    'FINANCE': false,
    'FUN': false,
  };

  // Function to generate a sum from the list of values and store in a listed array
  T sum<T extends num>(T lhs, T rhs) => lhs + rhs;
  List<int> _dailyScore = [];

  // get the checkbox results and add them into the list array
  getCheckboxResults()
  {
    values.forEach((key, value) {
      if(value == true)
      {
        _dailyScore.add(1);
      } else {
        _dailyScore.add(0);
      }
    });

    // Get the sum of the daily score result ready to send to the database
    int scoreResult = _dailyScore.reduce(sum);
    dailyScore.dailyScore = scoreResult.toString();

    if(scoreResult != null) {
      _sendScore();
    }

    // Clear the score array after use each time
    _dailyScore.clear();
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                    'This section is for your 5 daily things, please long press on each of the categories to create a task. Once complete make sure to check each one off.'
                ),
              ),
              Expanded(
                child :
                ListView(
                  children: values.keys.map((String key) {
                    return new CheckboxListTile(
                      secondary: IconButton(
                        icon: Icon(
                          Icons.info,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          widget.popup.show(
                            title: key,
                            content: 'popup text goes here - needs an array to loop via key',
                            actions: [
                              widget.popup.button(
                                label: 'Close',
                                onPressed: Navigator.of(context).pop,
                              ),
                            ],
                          );
                        },),
                      title: new Text(key),
                      value: values[key],
                      activeColor: Colors.red,
                      onChanged: (bool value){
                        setState(() {
                          values[key] = value;
                        });
                      },
                    );
                  }).toList(),
                ),
              ),
              RoundedButton(
                  title: 'Submit',
                  colour: Colors.redAccent,
                  onPressed: getCheckboxResults
              ),
            ],
          ),
        )
    );
  }

  //send the score to the database
  _sendScore() async {
    final uid = await Provider.of(context).auth.getCurrentUID();
    await db.collection("userData").document(uid).collection("score").add(dailyScore.toJson());
    Navigator.pushNamed(context, MainScreen.id);
  }
}