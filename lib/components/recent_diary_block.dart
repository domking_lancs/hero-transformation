import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hero_transformation/widgets/provider_widget.dart';
import 'package:intl/intl.dart';

class RecentDiaryBlock extends StatelessWidget {

  RecentDiaryBlock({this.title, this.blockText, this.blockTextColour, this.blockTextSize});

  final db = Firestore.instance;
  final String title;
  final String blockText;
  final Color blockTextColour;
  final double blockTextSize;
  dynamic today = new DateTime.now();

  @override
  Widget build(BuildContext context) {
    today = new DateTime(today.year, today.month, today.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: StreamBuilder(
            stream: getUsersRecentDiaryStreamSnapshots(context),
            builder: (context, snapshot) {
              if(snapshot.data == null) return CircularProgressIndicator();
              if(snapshot.data.documents.length == 0){
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15.0),
                  child: Text('You have no diary entries'),
                );
              } else {
                return Expanded(
                  child: SizedBox(
                      height: 50.0,
                      child: Center(
                        child: new ListView.builder(
                            shrinkWrap: true,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (BuildContext context, int index) =>
                                getDiary(context, snapshot.data.documents[index])),
                      )
                  ),
                );
              }
            },
          ),
        )
      ],
    );
  }

  // get all of the diary entries from Firebase
  Stream<QuerySnapshot> getUsersRecentDiaryStreamSnapshots(
      BuildContext context) async* {
    final uid = await Provider
        .of(context)
        .auth
        .getCurrentUID();
    yield* Firestore.instance.collection('userData').document(uid).collection(
        'diary').where("endDate", isGreaterThan: today).limit(1).snapshots();
  }

  // get all diary entries and display them in a list view
  Widget getDiary(BuildContext context, DocumentSnapshot diary) {
    String activity;

    if(diary['freeTimeActivity'] != null){
      activity = diary['freeTimeActivity'].toString();
    } else if(diary['freeTimeActivity'] == null && diary['projectTitle'] != null){
      activity = diary['projectTitle'].toString();
    } else {
      activity = diary['relationshipName'].toString();
    }

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: Text(
        "$activity - Scheduled between the dates: ${DateFormat('dd/MM/yyyy').format(diary['startDate'].toDate()).toString()} - ${DateFormat('dd/MM/yyyy').format(diary['endDate'].toDate()).toString()}",
        style: TextStyle(
            fontSize: blockTextSize ?? 12.0,
            color: blockTextColour
        ),
      ),
    );
  }
}